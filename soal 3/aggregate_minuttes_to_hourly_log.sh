#/bin/bash

date_time=$(date +"%Y%m%d%H")
output_path="/home/naufalrama/log/metrics_agg_$date_time.log"
location_path="/home/naufalrama/"

list_content_file() {
	for file in $(ls /home/naufalrama/log/metrics_2022* | grep $date_time)
	do 
		cat $file | grep -v mem 
	done
}

get_sorting() {
	if [ $1 == "mem_total" ]
	then
		index=1
	elif [ $1 == "mem_used" ]
	then
		index=2
	elif [ $1 == "mem_free" ]
	then
		index=3
	elif [ $1 == "mem_shared" ]
	then
		index=4
	elif [ $1 == "mem_buff" ]
	then
		index=5
	elif [ $1 == "mem_available" ]
	then
		index=6
	elif [ $1 == "swap_total" ]
	then
		index=7
	elif [ $1 == "swap_used" ]
	then
		index=8
	elif [ $1 == "swap_free" ]
	then
		index=9
	elif [ $1 == "path_size" ]
	then
		index=11
	fi

	list_content_file | awk -F , '
	{
		if(min == "") {
			min=max=$ind
		}
		if($ind > max) {
			max=$ind
		}
		if($ind < min) {
			min=$ind
		}
		
		total+=$ind;
		count+=1

	} END {
		if(choose == "max") {
			print max
		}
		if(choose == "min") {
			print min
		}
		if(choose == "avg") {
			average=total/count
			printf "%.0f", average
		}
	}' ind=$index choose=$2
}

# mem_total
mem_total_min=$(get_sorting mem_total min)
mem_total_max=$(get_sorting mem_total max)
mem_total_avg=$(get_sorting mem_total avg)

# mem_used
mem_used_min=$(get_sorting mem_used min)
mem_used_max=$(get_sorting mem_used max)
mem_used_avg=$(get_sorting mem_used avg)

# mem_free
mem_free_min=$(get_sorting mem_free min)
mem_free_max=$(get_sorting mem_free max)
mem_free_avg=$(get_sorting mem_free avg)

# mem_shared
mem_shared_min=$(get_sorting mem_shared min)
mem_shared_max=$(get_sorting mem_shared max)
mem_shared_avg=$(get_sorting mem_shared avg)

# mem_buff
mem_buff_min=$(get_sorting mem_buff min)
mem_buff_max=$(get_sorting mem_buff max)
mem_buff_avg=$(get_sorting mem_buff avg)

# mem_available
mem_available_min=$(get_sorting mem_available min)
mem_available_max=$(get_sorting mem_available max)
mem_available_avg=$(get_sorting mem_available avg)

# swap_total
swap_total_min=$(get_sorting swap_total min)
swap_total_max=$(get_sorting swap_total max)
swap_total_avg=$(get_sorting swap_total avg)

# swap_used
swap_used_min=$(get_sorting swap_used min)
swap_used_max=$(get_sorting swap_used max)
swap_used_avg=$(get_sorting swap_used avg)

# swap_free
swap_free_min=$(get_sorting swap_free min)
swap_free_max=$(get_sorting swap_free max)
swap_free_avg=$(get_sorting swap_free avg)

# path_size
path_size_min=$(get_sorting path_size min)
path_size_max=$(get_sorting path_size max)
path_size_avg=$(get_sorting path_size avg)

# printing
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> $output_path
echo "minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$location_path,$path_size_min" >> $output_path
echo "maximum,$mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$location_path,$path_size_max" >> $output_path
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$location_path,$path_size_avg" >> $output_path

chmod 700 $output_path

# Cronjob!
# * * * * * /home/naufalrama/shell/aggregate_minutes_to_hourly_log.sh

