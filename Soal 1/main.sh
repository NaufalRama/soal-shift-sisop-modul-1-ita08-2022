#!/bin/bash

log_path=./log.txt
db_path=./users/user.txt

read -p "username: " username
read -s -p "password: " password

user=$(grep "$username" $db_path)

if [ "$user" != "" ]; then
    correct_pass="$(echo $user | awk '{print $2}')"
    if [ $correct_pass != $password ]; then
        message="LOGIN: ERROR Failed login attempt on user $username"
        echo "$(date +"%m%d%y %H%M%S") $message" >> $log_path
        exit 1
    elif [ $correct_pass == $password ]; then
    	message="LOGIN: INFO User $username logged in"
        echo "$(date +"%m%d%y %H%M%S") $message" >> $log_path
    fi
fi

echo -e "\n"
read -p "Insert command (dl, att): " command

case "$command" in
	"dl")
		NOW=$(date +"%Y-%m-%d")
    mkdir "$NOW"
    for ((num=1; num<=$n; num++))
    do
        
        if [ $num -lt 10 ]
        then
            curl https://loremflickr.com/cache/resized/65535_51696326178_96581e9ab7_n_320_240_nofilter.jpg --output "PIC_0$num.jpg"
            mv "PIC_0$num.jpg" "$NOW/"
            # curl https://loremflickr.com/cache/resized/65535_51696326178_96581e9ab7_n_320_240_nofilter.jpg --output "PIC_0$num.jpg"
        else
            curl https://loremflickr.com/cache/resized/65535_51696326178_96581e9ab7_n_320_240_nofilter.jpg --output "PIC_$num.jpg"
            mv "PIC_$num.jpg" "$NOW/"
            # curl https://loremflickr.com/cache/resized/65535_51696326178_96581e9ab7_n_320_240_nofilter.jpg --output "PIC_$num.jpg"
        fi
    done
    password=""
    egrep "$password$" .users/user.txt >/dev/null

    zip -rP "$?" "$NOW.zip" "$NOW/"
		;;
	"att")
		successLog=$(awk -v var="$username" 'BEGIN{n=0} {if ($6 == var) n+=1} END{print n-1}' log.txt)
		failedLog=$(awk -v var="$username" 'BEGIN{n=0} {if ($NF == var) n+=1} END{print n}' log.txt)
		#echo -e "$successLog $failedLog\n"
		sum=$(($successLog+$failedLog))
		echo -e "Attempts: $sum \n"
		;;
	*)
		echo -e "Command doesn't exist!"
		exit 1
		;;
	esac
