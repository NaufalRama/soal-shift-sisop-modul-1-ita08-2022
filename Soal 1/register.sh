#!/bin/bash

if [ $(id -u) -eq 0 ]; then
	read -p "Enter username : " username
	read -s -p "Enter password : " password
    # passlength=${#passsword}
    b=8
    if [ ${#password} -lt $b ]
    then 
        echo -e "\npassword harus lebih dari sama dengan 8 karakter\n"
        # exit karena tidak sesuai kriteria
        exit 
    elif [ $username == $password ]
    then 
        echo -e "\nPassword tidak boleh sama dengan username\n"
        # exit karena tidak sesuai kriteria
        # exit 
    # elif [[ "$password" =~ [^a-zA-Z0-9] ]]
    elif [[ "$password" =~ [a-z] ]]
    then   
        if [[ "$password" =~ [A-Z] ]]
        then
            if [[ "$password" =~ [0-9] ]]
            then
                # echo -e "\nuser berhasil dibuat\n"
                #  User has been added to system

                egrep "^$username" .users/user.txt >/dev/null

                if [ $? -eq 0 ]; then
                    echo -e "\nREGISTER: ERROR User already exists"
                    # add date in log.txt
                    NOW=$(date +"%m/%d/%Y %T")
                    echo "$NOW  REGISTER: ERROR User already exists" >> .users/log.txt
                    exit 1
                else
                    pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
                    useradd -m -p "$pass" "$username"                                                           
                    echo "$username : $password" >> .users/user.txt
                    # add date in log.txt
                    NOW=$(date +"%m/%d/%Y %T")
                    echo "$NOW  REGISTER: INFO User $username registered successfully" >> .users/log.txt

                    [ $? -eq 0 ] && echo -e "\nUser has been added to system!" || echo -e "\nFailed to add a user!"
                    exit 
                fi

            else
                echo -e "\n password harus terdapat angka\n"
                # add date in log.txt
                NOW=$(date +"%m/%d/%Y %T")
                echo "$NOW" >> .users/log.txt
            fi
        else
            echo -e "\n password harus terdapat huruf besar\n" 
            # add date in log.txt
            NOW=$(date +"%m/%d/%Y %T")
            echo "$NOW" >> .users/log.txt
        fi    


    else
        echo -e "\n password harus terdapat huruf kecil"
        # add date in log.txt
        NOW=$(date +"%m/%d/%Y %T")
        echo "$NOW" >> .users/log.txt
        exit 
    fi
else
    echo "Only root may add a user to the system."
    # add date in log.txt
    NOW=$(date +"%m/%d/%Y %T")
    echo "$NOW" >> .users/log.txt
	exit 
fi