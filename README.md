# Soal-shift-sisop-modul-1-ITA08-2022

Anggota Kelompok :
1. Ilham Muhammad Sakti - 5027201042
2. Gennaro Fajar Mennde - 5027201061
3. Naufal Ramadhan - 5027201067

## Daftar Isi
- [Soal 1](#soal-1)
- [Soal 2](#soal-2)
- [Soal 3](#soal-3)

## Soal 1
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

### Soal 1. a
Han membuat sistem register pada script register.sh dan setiap user yang berhasil didaftarkan disimpan di dalam file ./users/user.txt. Han juga membuat sistem login yang dibuat di script main.sh

### Pembahasan 1. a
Pertama, kita membuat script pada visual studio code atau code editor apapun. Seperti yang diminta soal, kita membuat script `register.sh` sebagai tempat untuk tempat register atau pendaftaran pengguna baru atau akun baru. Kemudian membuat script `main.sh` sebagai tempat untuk login pengguna. Dan yang terakhir membuat file `./users/user.txt` sebagai tempat untuk menampung data pengguna baru seperti username dan password.

### Soal 1. b
Demi menjaga keamanan, input password pada login dan register harus tertutup/hidden dan password yang didaftarkan memiliki kriteria sebagai berikut :
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username

### Pembahasan 1. b
Pertama, untuk minimal 8 karakter kami menggunakan fungsi `if` dengan menggunakan `-lt` untuk memeriksa apakah nilai operan kiri lebih kecil daripada operan kanan. Nilai dalam kasus ini adalah mengecek password.
``` sh
if [ ${#password} -lt $b ]
    then 
        echo -e "\npassword harus lebih dari sama dengan 8 karakter\n"
        # exit karena tidak sesuai kriteria
        exit 
```

Kemudian, program akan mengecek apakah password tersebut memiliki kesamaan dengan username dengan menggunakan fungsi seperti bawah ini.
``` sh
elif [ $username == $password ]
    then 
        echo -e "\nPassword tidak boleh sama dengan username\n"
```

Dan yang terakhir, program akan mengecek apakah password yang dimasukan oleh user memiliki minimal 1 huruf kapital dan 1 huruf kecil dan apakah terdapat alphanumeric di dalam password tersebut.
``` sh
elif [[ "$password" =~ [a-z] ]]
    then   
        if [[ "$password" =~ [A-Z] ]]
        then
            if [[ "$password" =~ [0-9] ]]
            then
                # echo -e "\nuser berhasil dibuat\n"
                #  User has been added to system
```

Apabila password tidak memenuhi salah satu dari syarat diatas maka program akan secara otomatis memberikan output berupa error sesuai dengan syarat apa yang belum terpenuhi.

### Soal 1. c
Setiap percobaan login dan register akan tercatat pada log.txt dengan format : MM/DD/YY hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists.
- Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully.
- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME.
- Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in.

### Pembahasan 1. c
Untuk nomor 1 c dapat dilihat source code dibawah.
```sh
#!/bin/bash

log_path=./log.txt
db_path=./users/user.txt

read -p "username: " username
read -s -p "password: " password

user=$(grep "$username" $db_path)

if [ "$user" != "" ]; then
    correct_pass="$(echo $user | awk '{print $2}')"
    if [ $correct_pass != $password ]; then
        message="LOGIN: ERROR Failed login attempt on user $username"
        echo "$(date +"%m%d%y %H%M%S") $message" >> $log_path
        exit 1
    elif [ $correct_pass == $password ]; then
    	message="LOGIN: INFO User $username logged in"
        echo "$(date +"%m%d%y %H%M%S") $message" >> $log_path
    fi
fi
```
Jadi disini pertama-tama adalah mendefinisikan path log dan user. Kemudian, menerima inputan berupa username dan password. Setelah itu mencari username yang sesuai dengan username yang ada di path log. Jika sesuai maka kemudian dilanjutkan pengecekan password. Jika username dan password sesuai maka login akan berhasil dan jika sebaliknya maka akan menampilkan login gagal di `log.txt`.

### Soal 1. d
Setelah login, user dapat mengetikkan 2 command dengan dokumentasi sebagai berikut :
- dl N ( N = Jumlah gambar yang akan didownload)
Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.
- att
Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

### Pembahasan 1. d
Untuk nomor 1d seperti yang diminta oleh soal. User dapat mengetikkan 2 command yakni `dl` dan `att` dan source code seperti dibawah.
```sh
read -p "Insert command (dl, att): " command

case "$command" in
	"dl")
		NOW=$(date +"%Y-%m-%d")
    mkdir "$NOW"
    for ((num=1; num<=$n; num++))
    do
        
        if [ $num -lt 10 ]
        then
            curl https://loremflickr.com/cache/resized/65535_51696326178_96581e9ab7_n_320_240_nofilter.jpg --output "PIC_0$num.jpg"
            mv "PIC_0$num.jpg" "$NOW/"
            # curl https://loremflickr.com/cache/resized/65535_51696326178_96581e9ab7_n_320_240_nofilter.jpg --output "PIC_0$num.jpg"
        else
            curl https://loremflickr.com/cache/resized/65535_51696326178_96581e9ab7_n_320_240_nofilter.jpg --output "PIC_$num.jpg"
            mv "PIC_$num.jpg" "$NOW/"
            # curl https://loremflickr.com/cache/resized/65535_51696326178_96581e9ab7_n_320_240_nofilter.jpg --output "PIC_$num.jpg"
        fi
    done
    password=""
    egrep "$password$" .users/user.txt >/dev/null

    zip -rP "$?" "$NOW.zip" "$NOW/"


		;;
	"att")
		successLog=$(awk -v var="$username" 'BEGIN{n=0} {if ($6 == var) n+=1} END{print n-1}' log.txt)
		failedLog=$(awk -v var="$username" 'BEGIN{n=0} {if ($NF == var) n+=1} END{print n}' log.txt)
		#echo -e "$successLog $failedLog\n"
		sum=$(($successLog+$failedLog))
		echo -e "Attempts: $sum \n"
		;;
	*)
		echo -e "Command doesn't exist!"
		exit 1
		;;
	esac

```
Pertama-tama sistem akan menerima inputan yang akan diinputkan oleh user. Kemudian, dicek apakah command tersebut `dl` jika iya maka akan menjalankan perintah download gambar. Jika inputan berupa `att`, maka akan menjalankan perintah menghitung login yang berhasil maupun tidak yang sedang login saat ini. 



## Soal 2
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya. Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas berikut:

### Soal 2. a
Buat folder terlebih dahulu bernama forensic_log_website_daffainfo_log.

### Pembahasan 2. a
Pada soal 2.a untuk membuat folder baru bernama forensic_log_website_daffainfo_log, kita hanya perlu mengetikkan :
``` sh
mkdir forensic_log_website_daffainfo_log
```

### Soal 2. b
Dikarenakan serangan yang diluncurkan ke website https://daffa.info sangat banyak, Dapos ingin tahu berapa rata-rata request per jam yang dikirimkan penyerang ke website. Kemudian masukkan jumlah rata-ratanya ke dalam sebuah file bernama ratarata.txt ke dalam folder yang sudah dibuat sebelumnya.

### Pembahasan 2. b
Untuk soal 2.b, kita hanya butuh menghitung jumlah jam dan jumlah request pada log. Untuk mengetahui rata-rata request perjam, cukup dihitung jumlah request total dan dibagi dengan jumlah jam mulai dari request paling awal hingga request paling akhir. Untuk programmnya dapat dituliskan sebagai berikut:
``` sh
awk -F: '
    {
        $3 n++
    }
    END {
        print "Rata-rata serangan adalah sebanyak "(n-1)/12 " requests per jam"
    }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt
```
Setelah didapatkan rata-rata request, data rata-rata request tersebut akan dimasukkan ke forensic_log_website_daffainfo_log/ratarata.txt

### Soal 2. c
Sepertinya penyerang ini menggunakan banyak IP saat melakukan serangan ke website https://daffa.info, Dapos ingin menampilkan IP yang paling banyak melakukan request ke server dan tampilkan berapa banyak request yang dikirimkan dengan IP tersebut. Masukkan outputnya kedalam file baru bernama result.txt kedalam folder yang sudah dibuat sebelumnya.

### Pembahasan 2. c
Untuk soal 2.c, kita dapat mencari IP yang paling banyak melakukan request ke server dan jumlah requestnya dengan cara : Pertama, kita lakukan sort IP. Kemudian, kita list jenis IP yang berbeda dan jumlah munculnya dengan `uniq -c`. Kemudian sortir lagi berdasarkan banyak request dan kita ambil yang paling atas. Kodenya adalah:
``` sh
awk -F: '{print "IP yang paling banyak mengakses server adalah adalah " $1 " sebanyak "}' log_website_daffainfo.log | sort | uniq -c | sort -nr | head --lines=1 >> forensic_log_website_daffainfo_log/result.txt
```

### Soal 2. d
Beberapa request ada yang menggunakan user-agent ada yang tidak. Dari banyaknya request, berapa banyak requests yang menggunakan user-agent curl?
Kemudian masukkan berapa banyak requestnya kedalam file bernama result.txt yang telah dibuat sebelumnya.

### Pembahasan 2. d
Untuk soal 2.d kita melakukan pencarian dengan menggunakan cari kata kunci curl dan menghitung jumlah line yang terdapat curl. Penyelesaian dapat dibuat dengan kode sebagai berikut:
``` sh
awk '
    /curl/ { ++n }
    END { print "Ada " n " requests yang menggunakan curl sebagai user-agent"}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```

### Soal 2. e
Pada jam 2 pagi pada tanggal 22 terdapat serangan pada website, Dapos ingin mencari tahu daftar IP yang mengakses website pada jam tersebut. Kemudian masukkan daftar IP tersebut kedalam file bernama result.txt yang telah dibuat sebelumnya.

### Pembahasan 2. e
Soal 2.e dapat diselesaikan dengan menlist semua request pada jam 2. Penyelesaian dapat dibuat dengan kode sebagai berikut:
``` sh
awk -F: '$3 == 02 {print $1 " Jam 2 pagi"}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```


## Soal 3
Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh <target_path>` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

### Soal 3. a
Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.

### Pembahasan 3. a
Pertama, kita dapat memasukkan command `free -m` pada terminal untuk mengecek penggunaan RAM dan pada terminal akan ditunjukkan penggunaan RAM pada waktu tersebut. Kemudian kita dapat menggunakan command `du -sh` untuk mengecek penggunaan disk.

Kemudian setelah mendapatkan data dari command yang sudah didapatkan sebelumnya dapat diolah menggunakan konsep **AWK** untuk mengolah data tersebut menjadi baris-perbaris seperti dibawah.
```
mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size
15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M
```
Kemudian, karena semua data diatas akan dimasukkan dalam sebuah file .log yang memiliki ketentuan seperti contoh metrics_20220131150000.log. Maka pada program diawali membuat folder yang berisikan file tersebut seperti berikut.
``` sh
mkdir -p /home/naufalrama/log
date_time=$(date +"%Y%m%d%H%M%S")
output_path="/home/naufalrama/log/metrics_$date_time.log"
``` 
Command `mkdir` akan membuat sebuah folder sesuai dengan path yang telah ditentukan dan berisikan file dengan ketentuan yang sudah ada seperti `metrics_$date_time.log`.

Setelah itu, kita dapat melakukan print pada string menggunakan perintah `echo` dan output file tersebut akan tersimpan dalam `$output_path` yang sudah kita tentukan sebelumnya menggunakan `>>`. 
``` sh
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> $output_path
``` 

Kemudian, kita menggunakan perintah `free -m` untuk mendapatkan data yang berisikan memory, swap, dan storage. Data tersebut akan diteruskan dan diolah oleh perintah **AWK** dan argumen akan di print yang kemudian akan disimpan ke path yang telah ditentukan. Seperti code dibawah ini.
``` sh
memory="$(free | awk '/Mem:/ {printf "%s,%s,%s,%s,%s,%s", $2,$3,$4,$5,$6,$7}')"
swap="$(free | awk '/Swap:/ {printf "%s,%s,%s", $2,$3,$4}')"
storage="$(du -sh /home/naufalrama/ | awk '{printf "%s,%s",$2,$1}')"
echo "$memory,$swap,$storage" >> $output_path
```
Untuk bagian memory yang akan di print hanya argumen `$2`, `$3`, `$4`, `$5`, `$6`, `$7` dan output akan disimpan pada variable `memory`.

Pada bagian swap yang akan di print hanya argumen `$2`, `$3`, `$4` yang kemudian akan disimpan dalam variable `swap`.

Dan yang terakhir, bagian storage yang menggunakan perintah `du -sh` hanya akan di print argumen `$2` dan `$1` dan akan disimpan pada variable `storage`.


Pada akhir script, kita akan mengubah akses file tersebut sehingga yang dapat mengakses hanya user menggunakan perintah `chmod` seperti dibawah.
``` sh
chmod 700 $output_path
```

### Soal 3. b
Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.

### Pembahasan 3. b
Untuk nomor 3b ini, seperti yang diminta sebelumnya pada script minutelog.sh yaitu harus berjalan setiap menit. Maka kita menggunakan `cron` untuk mencetak log pada tiap menit. Mula-mula, kita gunakan `crontab -e` untuk membuat crontab baru dan kita masukkan `* * * * * bash /home/naufalrama/shell/minute_log.sh` yang tandanya shell tersebut kita jalanan pada tiap menit. Hasil akhir dari program tersebut adalah berupa file.log yang memiliki contoh nama seperti metrics_20220131150000.log.

### Soal 3. c
Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log

### Pembahasan 3. c
Seperti yang diminta oleh soal, pertama kita membuat script shell dengan nama aggregate_minutes_to_hourly_log.sh. File tersebut akan menghasilkan sebuah file.log yang isinya berupa metrics_agg_{YmdH}.log. Sehingga kita harus membuat variable yang akan menyimpang .log tersebut dan pathnya.
``` sh
date_time=$(date +"%Y%m%d%H")
output_path="/home/naufalrama/log/metrics_agg_$date_time.log"
location_path="/home/naufalrama/"
```

Nantinya, script aggregate_minutes_to_hourly_log.sh akan berisikan file yang diproses setiap menitnya sehingga kita perlu mendapatkan file-file tersebut dengan menggunakan `list_content_file()` sesuai dengan code berikut.
``` sh
list_content_file() {
	for file in $(ls /home/naufalrama/log/metrics_2022* | grep $date_time)
	do 
		cat $file | grep -v mem 
	done
}
```
Setelah itu, kita mencoba untuk memproses `list_content_file()` tersebut dengan menggunakan fungsi `get_sorting()` yang akan mengeprint nilai minimum, maksimum, dan average (rata-rata) sesuai dengan kategorinya. Pada awal fungsi tersebut, kita harus melakukan sebuah perkondisian sesuai dengan argumennya terlebih dahulu. Untuk perkondisian seperti dibawah ini.
``` sh
get_sorting() {
	if [ $1 == "mem_total" ]
	then
		index=1
	elif [ $1 == "mem_used" ]
	then
		index=2
	elif [ $1 == "mem_free" ]
	then
		index=3
	elif [ $1 == "mem_shared" ]
	then
		index=4
	elif [ $1 == "mem_buff" ]
	then
		index=5
	elif [ $1 == "mem_available" ]
	then
		index=6
	elif [ $1 == "swap_total" ]
	then
		index=7
	elif [ $1 == "swap_used" ]
	then
		index=8
	elif [ $1 == "swap_free" ]
	then
		index=9
	elif [ $1 == "path_size" ]
	then
		index=11
	fi
```
Kemudian kita melakukan operasi **AWK** untuk mengolah data yang telah didapatkan sebelumnya. Tetapi, sebelum mengolah data tersebut. Kita harus pass terlebih dahulu 2 variable yakni `ind` sesuai dengan nilai `$index` dan pilihan sort melalui variable `choose` sesuai dengan pemanggilan fungsi. Kemudian di akhir akan dilakukaan perkondisian dengan menggunakan variable `total` dan variable `count` berdasarkan banyaknya perulangan yang terjadi. Seperti code berikut.
``` sh
list_content_file | awk -F , '
	{
		if(min == "") {
			min=max=$ind
		}
		if($ind > max) {
			max=$ind
		}
		if($ind < min) {
			min=$ind
		}
		
		total+=$ind;
		count+=1

	} END {
		if(choose == "max") {
			print max
		}
		if(choose == "min") {
			print min
		}
		if(choose == "avg") {
			average=total/count
			printf "%.0f", average
		}
	}' ind=$index choose=$2
}
```

Dengan command diatas makaa kita akan mendapatkan data-data sesuai dengan fungsi dan pass sesuai argumen yang sesuai. Seperti code berikut.
``` sh
# mem_total
mem_total_min=$(get_sorting mem_total min)
mem_total_max=$(get_sorting mem_total max)
mem_total_avg=$(get_sorting mem_total avg)

# mem_used
mem_used_min=$(get_sorting mem_used min)
mem_used_max=$(get_sorting mem_used max)
mem_used_avg=$(get_sorting mem_used avg)

# mem_free
mem_free_min=$(get_sorting mem_free min)
mem_free_max=$(get_sorting mem_free max)
mem_free_avg=$(get_sorting mem_free avg)

# mem_shared
mem_shared_min=$(get_sorting mem_shared min)
mem_shared_max=$(get_sorting mem_shared max)
mem_shared_avg=$(get_sorting mem_shared avg)

# mem_buff
mem_buff_min=$(get_sorting mem_buff min)
mem_buff_max=$(get_sorting mem_buff max)
mem_buff_avg=$(get_sorting mem_buff avg)

# mem_available
mem_available_min=$(get_sorting mem_available min)
mem_available_max=$(get_sorting mem_available max)
mem_available_avg=$(get_sorting mem_available avg)

# swap_total
swap_total_min=$(get_sorting swap_total min)
swap_total_max=$(get_sorting swap_total max)
swap_total_avg=$(get_sorting swap_total avg)

# swap_used
swap_used_min=$(get_sorting swap_used min)
swap_used_max=$(get_sorting swap_used max)
swap_used_avg=$(get_sorting swap_used avg)

# swap_free
swap_free_min=$(get_sorting swap_free min)
swap_free_max=$(get_sorting swap_free max)
swap_free_avg=$(get_sorting swap_free avg)

# path_size
path_size_min=$(get_sorting path_size min)
path_size_max=$(get_sorting path_size max)
path_size_avg=$(get_sorting path_size avg)
```
Dan diakhiri dengan print semua variable dan disimpan pada `$ouput_path`. Seperti berikut.
``` sh
# printing
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> $output_path
echo "minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$location_path,$path_size_min" >> $output_path
echo "maximum,$mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$location_path,$path_size_max" >> $output_path
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$location_path,$path_size_avg" >> $output_path
```

### Soal 3. d
Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

### Pembahasan Soal 3. d
Untuk dapat membuat file log dapat dibuka oleh user, kita dapat menggunakan command seperti code dibawah ini.
``` sh
chmod 700 $output_path
```
Command `chmod` memiliki fungsi untuk mengubah perizinan user, kemudian angka tujuh pertama untuk menjadi user dapat melakukan read, write, dan execute pada file, angka 0 kedua untuk menjadikan group user tidak memiliki akses terhadap file, angka 0 ketiga untuk menjadikan orang lain/other juga tidak memiliki akses terhadap file. dan kemudian disimpan dalam `$output_path`. 
