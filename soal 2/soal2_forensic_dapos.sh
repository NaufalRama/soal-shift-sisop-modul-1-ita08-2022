#!/bin/bash

# # 2a
mkdir forensic_log_website_daffainfo_log

# 2b
awk -F: '
    {
        $3 n++
    }
    END {
        print "Rata-rata serangan adalah sebanyak "(n-1)/12 " requests per jam"
    }
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt

# 2c
awk -F: '{print "IP yang paling banyak mengakses server adalah adalah " $1 " sebanyak "}' log_website_daffainfo.log | sort | uniq -c | sort -nr | head --lines=1 >> forensic_log_website_daffainfo_log/result.txt

# 2d
awk '
    /curl/ { ++n }
    END { print "Ada " n " requests yang menggunakan curl sebagai user-agent"}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

# 2e
# menampilkan ip semua jam lengkap
awk -F: '$3 == 02 {print $1 " Jam 2 pagi"}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

